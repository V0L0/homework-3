//
//  ViewController.swift
//  HomeWork 3
//
//  Created by Volodymyr on 4/17/19.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // myName("Vladimir")
        // myPatronymic("Andreyvych")
        // separator("VladimirAndreyvych")
        // reversString("ОсьУ")
        // formatDigitalString("1648122343556577")
        // checkPassword("sde")
        // arraySort()
        // convertStrToTranslite("СИСТЕМА")
        // letterSearch()
         antimat()
    }
    //Задача 1. Создать строку с своим именем, вывести
    //          количество символов содержащихся в ней.
    func myName(_ receivedName: String) -> Int {
        var i = 0
        for _ in receivedName.indices {
            i += 1
        }
        print("Меня зовет: \(receivedName), в этом имени \(i) букв")
        return i
    }
    //Задача 2. Создать строку с своим отчеством проверить его на окончание “ич/на”
    func myPatronymic(_ receivedString: String) -> String {
        var male: String
        if receivedString.hasSuffix("ych") {
        male = ("male")
        }
        else {
        male = ("female")
        }
        print("\(receivedString) - \(male)" )
        return male
    }
    //Задача 3. Cоздать строку, где слитно написано Ваши ИмяФамилия
    // проверка буквы на ее регистер
    func isLower(char: Character) -> Bool {
        let checkChar = String(char)
        return (checkChar == checkChar.lowercased()) && (checkChar != checkChar.uppercased())
    }
    func isUpper(char: Character) -> Bool {
        let checkChar = String(char)
        return (checkChar != checkChar.lowercased()) && (checkChar == checkChar.uppercased())
    }
    func separator(_ someStr: String) ->String {
        var strArr = [Character]()
        for character in someStr {
            strArr += [character]
        }
        let countOfString = strArr.count
        var nameEnd = 0
        for i in 1..<countOfString {
            if true != isLower(char: strArr[i]) {
                nameEnd = i
            }
        }
        let str = someStr
        let someNsStr = str as NSString
        let rangeName =  NSRange(location: 0, length: nameEnd)
        let str1 = someNsStr.substring(with: rangeName)
        let rangePatronymic = NSRange(location: nameEnd, length: countOfString - nameEnd)
        let str2 = someNsStr.substring(with: rangePatronymic)
        
        print(str1)
        print(str2)
        let str3 = str1 + " " + str2
        print(str3)
        return str3
    }
    //Задача 4. Вывести строку зеркально Ось → ьсО не используя reverse (посимвольно)
    func reversString(_ someString: String) ->String {
    let countOfString = someString.count
    var someArr = [Character]()
    for character in someString {
        someArr  += [character]
    }
    var j = countOfString - 1
    var reversArr = [Character]()
    for _ in 0..<countOfString {
        reversArr.append(someArr[j])
        j -= 1
    }
    let reversStr: [Character] = reversArr
    let revString = String(reversStr)
    print("Исходное слово \(someString), реверс слова - \(revString)")
    return revString
    }
    //Задача 5. Добавить запятые в строку как их расставляет калькулятор
    func formatDigitalString (_ someDigitStr: String) -> String {
        let countOfString = someDigitStr.count
        var someArr = [Character]()
        for character in someDigitStr {
            someArr += [character]
        }
        print("Исходная строка : \(someDigitStr)")
        
        for index in stride(from: countOfString, to: 0, by: -3) {
            if index != countOfString {
                someArr.insert(",", at: index)
            }
        }
        let formArr: [Character] = someArr
        let formStr = String(formArr)
        print("Форматированная строка: \(formStr)")
        return formStr
    }
    
    // Задача 6. Проверить пароль на надежность от 1 до 5
 
    func checkPassword(_ inputPass: String) -> Int {
        var inputPassArr = [Character]()
        for character in inputPass {
            inputPassArr += [character]
        }
        let countOfString = inputPassArr.count
        var reliabilityOfPassword = 0
        var numbArr = [Character]()
        let numbStr = "0123456789"
        for character in numbStr {
            numbArr += [character]
        }
        var numCode = 0
        for i in 0..<countOfString {
            if numbArr.contains(inputPassArr[i]) {
                numCode += 1
                break
            }
        }
        if numCode == 1 {
            print("есть цифры")
            reliabilityOfPassword += 1
        }
        for i in 0..<countOfString {
            if isLower(char: inputPassArr[i]) {
                reliabilityOfPassword += 1
                print("есть меленькие буквы")
                break
            }
        }
        for i in 0..<countOfString {
            if isUpper(char: inputPassArr[i]) {
                reliabilityOfPassword += 1
                print("есть большие буквы")
                break
            }
        }
        let chars =  "\\ /@!&^%$#)(*&^"
        var noSpecialSymbols = [Character]()
        for character in chars {
            noSpecialSymbols += [character]
        }
        var nomeX = 0
        for i in 0..<countOfString {
            if noSpecialSymbols.contains(inputPassArr[i]) {
                nomeX += 1
                break
            }
        }
        if nomeX == 1 {
            print("есть спец символы")
            reliabilityOfPassword += 1
        }
        
        if reliabilityOfPassword == 4 {
            reliabilityOfPassword += 1
            print("уровень сложности пароля: \(reliabilityOfPassword)")
        }
        else {
            print("уровень сложности пароля: \(reliabilityOfPassword)")
        }
        return reliabilityOfPassword
}
    // Задача 7. Сортировка массива не встроенным методом по возрастанию +
    // удалитьдубликаты
    // генерим массив с задданными от и до границами
    func creatureArray(from: Int, to: Int) -> [Int] {
        var someArr = [Int]()
        for _ in from...to {
            someArr.append(Int.random(in: 0...9))
        }
        return someArr
    }
    
    func arraySort() {
        var arraySort = creatureArray(from: 1, to: 20)
        print("Сгенерированный массив:\n\(arraySort)")
        let set = Set(arraySort)
        var arrFiltr = Array(set)
        print("Удалены дубликаты :\n\(arrFiltr)")
        var countOfArraySort = arrFiltr.count
        var tempNump = 0
        for i in 0..<countOfArraySort - 1 {
            for j in 0..<countOfArraySort - i - 1 {
                if arrFiltr[j] > arrFiltr[j + 1] {
                    tempNump = arrFiltr[j]
                    arrFiltr[j] = arrFiltr[j + 1]
                    arrFiltr[j + 1] = tempNump
                }
            }
        }
        print("Отсортированный массив:\n\(arrFiltr)")
    }

    // Задача 8. Написать метод, который будет переводить строку в транслит.
    
    func convertStrToTranslite(_ someString: String) -> String {
        let translite: [Character : Character] = ["А": "A", "Б": "B", "В": "V", "Г": "G", "Д": "D", "Е": "E", "Ё": "Y", "Ж": "H", "З": "Z", "И": "I", "Й": "J", "К": "K", "Л": "L", "М": "M", "Н": "N", "О": "O", "П": "P", "Р": "R", "С": "S", "Т": "T"]
        var arrayOfSomeString = [Character]()
        for character in someString {
            arrayOfSomeString += [character]
        }
        var resultArray = [Character]()
        let countOfArraySomeString = arrayOfSomeString.count
        for i in 0..<countOfArraySomeString {
            for (transliteCode, transliteName) in translite {
                if transliteName == arrayOfSomeString[i] {
                    resultArray.append(transliteCode)
                }
                if transliteCode == arrayOfSomeString[i] {
                    resultArray.append(transliteName)
                }
            }
        }
        let stringRepresentation = String(resultArray)
        print("введенное слово \(someString) --> \(stringRepresentation) слово в транслите")
        return stringRepresentation
    }
    // Задача 9. Сделать выборку из массива строк в которых содержится указанная строка
    // [“lada”, “sedan”, “baklazhan”] search “da”
    func letterSearch() -> Array<Any> {
        let someArray =  ["lada", "sedan", "baklazhan"]
        var resultArray = [String]()
        _ = ["da"]
        for word in someArray {
            if word.contains("da") {
                resultArray.append(word)
            }
        }
        return resultArray
    }
   // Задача 10. Set<String> - antimat [“fuck”, “fak”] “hello my fak” “hello my ***”
    func antimat() {
        let antimat: Set<String> = ["fuck", "fak"]
        var someString: String = "hello my fak"
        var arraySomeString = [String]()
        var arSomeStr = someString.split(separator: " ")
        for word in arSomeStr {
            arraySomeString.append(String(word))
        }
        print(arraySomeString)
        let countOfarraySomeString = arraySomeString.count
       // print(countOfarraySomeString)
        var x = 0
        for i in 0..<countOfarraySomeString {
            if antimat.contains(arraySomeString[i]) {
              x = i
                print(x)
            }
        }
        arraySomeString.remove(at: x)
        arraySomeString.insert("***", at: x)
        print(arraySomeString)
        
           
}
}
